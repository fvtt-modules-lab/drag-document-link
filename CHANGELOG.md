## [1.0.4] - 2023-06-05

Fixed header drag override

## [1.0.3] - 2022-12-22

Add more document types

## [1.0.2] - 2022-12-22

Minor release version to fix links

## [1.0.1] - 2022-12-22

Minor release version to fix links

## [1.0.0] - 2022-12-22

First release
