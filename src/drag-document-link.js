function sheetHook(sheet, html) {
  const link = html.find(".window-title .document-id-link");
  if (link.attr("draggable")) return;

  // Modify element
  link.attr("draggable", "true");
  link.find("i").attr("class", "fa-solid fa-link");
  link.attr(
    "data-tooltip",
    `${link.attr("data-tooltip")} (${game.i18n.localize("CONTROLS.DragDrop")})`
  );

  // Stop header drag
  link.on("pointerdown", (event) => event.stopPropagation());
  // Inject drag data
  link.on("dragstart", (event) => {
    event.originalEvent?.dataTransfer?.setData(
      "text/plain",
      JSON.stringify(sheet.document.toDragData())
    );
  });
}

Hooks.on("renderActorSheet", sheetHook);
Hooks.on("renderItemSheet", sheetHook);
Hooks.on("renderJournalSheet", sheetHook);
Hooks.on("renderRollTableConfig", sheetHook);
Hooks.on("renderSceneConfig", sheetHook);
Hooks.on("renderCardsConfig", sheetHook);
Hooks.on("renderPlaylistConfig", sheetHook);
