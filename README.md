# Drag Document Link

Makes the document link in sheet headers draggable, to drop the document somewhere else.
